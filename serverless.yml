service: pageToPdf

frameworkVersion: ">=1.1.0 <2.0.0"

provider:
  name: aws
  runtime: nodejs12.x
  stage: ${opt:stage, 'develop'}
  region: eu-west-1
  versionFunctions: false
  deploymentBucket:
    name: ${file(env.yml):deploymentS3Bucket.${self:provider.stage}}
  memorySize: 512
  timeout: 30
  vpc: ${file(env.yml):vpc.${self:provider.stage}}
  iamRoleStatements:
    - Effect: "Allow"
      Action:
        - "s3:ListBucket*"
        - "s3:HeadObject"
        - "s3:GetObject"
        - "s3:GetObjectAcl"
        - "s3:PutObject"
        - "s3:PutObjectAcl"
        - "s3:DeleteObject"
      Resource:
        - "arn:aws:s3:::${file(env.yml):s3ServiceBucket.${self:provider.stage}}"
        - "arn:aws:s3:::${file(env.yml):s3ServiceBucket.${self:provider.stage}}/*"
    - Effect: Allow
      Action:
        - dynamodb:GetItem
        - dynamodb:PutItem
        - dynamodb:UpdateItem
        - dynamodb:DeleteItem
      Resource: "arn:aws:dynamodb:${opt:region, self:provider.region}:*:table/${self:provider.environment.dynamoDbTable}"
    - Effect: "Allow"
      Action:
        - "sqs:SendMessage"
      Resource:
        - "${ssm:edition_queue_arn~true}"
        - "${ssm:pages_queue_arn~true}"
        - "${ssm:generate_edition_queue_arn~true}"
  environment:
    baseUrl: ${file(env.yml):baseUrl.${self:provider.stage}}
    articleBaseUrl: ${file(env.yml):articleBaseUrl.${self:provider.stage}}
    s3DownloadUrl: ${file(env.yml):s3DownloadUrl.${self:provider.stage}}
    s3ServiceBucket: ${file(env.yml):s3ServiceBucket.${self:provider.stage}}
    s3ServiceRootFolder: ${file(env.yml):s3ServiceRootFolder.${self:provider.stage}}
    dynamoDbTable: ${ssm:edition_download_tracking_table~true}
    browserlessWSEndpoint: ${file(env.yml):browserlessWSEndpoint.${self:provider.stage}}
    htmlTemplateCSSStylesheet: ${file(env.yml):htmlTemplateCSSStylesheet.${self:provider.stage}}
    editionProcessQueue: ${ssm:edition_queue_url~true}
    pagesProcessQueue: ${ssm:pages_queue_url~true}
    editionGenerationQueue: ${ssm:generate_edition_queue_url~true}
  apiGateway:
    binaryMediaTypes:
      - "*/*"

functions:
  download:
    handler: download-pdf/initiateEdition.initiateEditionProcessing
    events:
      - http:
          path: pages/{editionGuid}
          method: GET
          cors: true
  triggerPageProcessing:
    handler: download-pdf/triggerPageProcessing.triggerPageProcessing
    events:
      - sqs:
          arn: ${ssm:edition_queue_arn~true}
  pageProcessing:
    handler: download-pdf/downloadFiles.downloadFiles
    timeout: 60
    events:
      - sqs:
          arn: ${ssm:pages_queue_arn~true}
          batchSize: 3
  downloadTracker:
    handler: download-pdf/pageTracker.downloadTracker
    events:
      - stream:
          type: dynamodb
          batchSize: 100
          batchWindow: 10
          maximumRetryAttempts: 5
          arn: ${ssm:edition_download_tracking_table_stream_arn~true}
