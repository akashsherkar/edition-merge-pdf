
'use strict';

const pkgPuppeteer = require('puppeteer-core');
const Hotspot = require('../classes/Hotspot');

module.exports = class PSPuppeteer {
    constructor(htmlUrl) {
        this.htmlUrl = htmlUrl; // the HTML URL that we will use going forward
        this.browser; // the Puppeteer 'Browser' object
        this.page; // the Puppeteer 'Page'
        this.pageDimensions = { width: 0, height: 0 }; // an object with the dimensions of the rendered HTML page within Browserless
    }

    async init() {
        console.log('Connecting to Puppetter...');
        this.browser = await pkgPuppeteer.connect({
            browserWSEndpoint: process.env.browserlessWSEndpoint
        });
        console.log(process.env.browserlessWSEndpoint);
        console.log('...Done!');

        console.log('Creating new Puppetter page...');
        this.page = await this.browser.newPage();
        this.page.on('console', msg => console.log('CONSOLE LOG:', msg.text()));
        this.page.emulateMedia('screen');
        console.log('...Done!');

        console.log(`Navigating to ${this.htmlUrl}...`);
        await this.page.goto(this.htmlUrl, { timeout: 45000, waitUntil: 'networkidle0' });
        console.log('...Done!');
    }

    async setupPageDimensions() {
        console.log('Preparing to evaluate web page.');
        this.pageDimensions = await this.page.evaluate(async () => {
            return {
                width: document.body.scrollWidth,
                height: document.body.scrollHeight
            };
        });
        console.log('Finished evaluating web page.');

        console.log('Setting viewport.');
        await this.page.setViewport({
            width: this.pageDimensions['width'],
            height: this.pageDimensions['height']
        });
        console.log('Finished setting viewport.');
    }

    async generatePDFBuffer() {
        console.log('Generating PDF buffer from HTML page.');
        let buffPDF = await this.page.pdf({
            printBackground: true,
            width: this.pageDimensions['width'],
            height: this.pageDimensions['height']
        });
        console.log('Finished generating PDF buffer from HTML page.');

        return buffPDF;
    }

    async generateImageBuffer(type, quality) {
        console.log('Generating Image buffer from HTML page.');
        let buffImage = await this.page.screenshot({
            type: type,
            quality: quality,
            fullPage: true
        });
        console.log('Finished generating Image buffer from HTML page.');

        return buffImage;
    }

    async extractHotspots() {
        console.log('Preparing to extract hotspots.');
        let arrTemp = await this.page.evaluate(async (pageDimensions) => {
            let arrArticleHotspots = [];
            const arrArticles = Array.from(document.querySelectorAll('.article'));
            for (const article of arrArticles) {
                arrArticleHotspots.push({
                    type: 'ARTICLE',
                    headline: article.querySelector('.headline').innerHTML,
                    x: article.offsetLeft / pageDimensions.width * 100,
                    y: article.offsetTop / pageDimensions.height * 100,
                    width: article.offsetWidth / pageDimensions.width * 100,
                    height: article.offsetHeight / pageDimensions.height * 100
                });
            }

            let arrHyperlinkHotspots = [];
            const arrHyperlinks = Array.from(document.querySelectorAll('.hyperlink'));
            for (const hyperlink of arrHyperlinks) {
                if (hyperlink.dataset["hyperlinkUrl"].includes('{{{') === false) {
                    arrHyperlinkHotspots.push({
                        type: 'HYPERLINK',
                        url: hyperlink.dataset["hyperlinkUrl"],
                        x: hyperlink.offsetLeft / pageDimensions.width * 100,
                        y: hyperlink.offsetTop / pageDimensions.height * 100,
                        width: hyperlink.offsetWidth / pageDimensions.width * 100,
                        height: hyperlink.offsetHeight / pageDimensions.height * 100
                    });
                }
            }

            return arrArticleHotspots.concat(arrHyperlinkHotspots);
        }, this.pageDimensions);
        console.log('Finished extracting hotspots.');

        let arrModifiedTemp = arrTemp.map(objHotspot => {
            let objTempHotspot = new Hotspot(objHotspot.x, objHotspot.y, objHotspot.width, objHotspot.height);
            switch (objHotspot.type.toUpperCase()) {
                case 'ARTICLE':                    
                    objTempHotspot.generateArticleType(objHotspot.headline);                    
                    break;
                case 'HYPERLINK':
                    objTempHotspot.generateHyperlinkType(objHotspot.url);
                    break;
            }
            return objTempHotspot;
        });

        return arrModifiedTemp;
    }

    async close() {
        console.log('Attempting to close our Puppeteer connection...');

        try {
            await this.page.close();
            await this.browser.close();

            console.log('...Done!');
        } catch (err) {
            console.log('Error while attempting to close our connection!');
            console.log(err);
        }
    }
}