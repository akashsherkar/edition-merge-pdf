'use strict';

module.exports = class Hotspot {
    constructor(xPos, yPos, width, height) {
        this.type = null;
        this.position = {
            x: xPos,
            y: yPos
        }; 
        this.size = {
            width: width,
            height: height
        };       
        this.data = {
            headline: null,
            url: null
        }
    }

    generateArticleType(headline) {
        this.type = 'ARTICLE';
        this.data.headline = headline;
    }

    generateHyperlinkType(url) {
        this.type = 'HYPERLINK';
        this.data.url = url;
    }
}