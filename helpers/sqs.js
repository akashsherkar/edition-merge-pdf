'use strict';

const pkgAWS = require('aws-sdk');
const pkgSQS = new pkgAWS.SQS({ region: 'eu-west-1' });

function generateQueueRequestObject(payload,messageGroupId, queue_url) {

    return {
        MessageBody: JSON.stringify(payload),
        MessageGroupId: messageGroupId,
        QueueUrl: queue_url
    };
}

exports.createSQSRecord = async function (payload ,messageGroupId, queue_name) {
    const objQueueRequest = generateQueueRequestObject(payload,messageGroupId, queue_name);
    // console.log(objQueueRequest);
    let objResponse;
    objResponse = await pkgSQS.sendMessage(objQueueRequest).promise();
    return objResponse;
}

exports.deleteSQSRecord = async function (payload) {
    const objQueueRequest = generateQueueRequestObject(payload, 'delete');
    let objResponse;
    objResponse = await pkgSQS.sendMessage(objQueueRequest).promise();
    return objResponse;
};