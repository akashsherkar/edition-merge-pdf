'use strict';

const pkgCrypto = require('crypto');

exports.generateHashFromString = function (str) {
    return pkgCrypto.createHash('sha1').update(str).digest('hex');
}