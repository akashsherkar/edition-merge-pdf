'use strict';

const pkgAWS = require('aws-sdk');
const pkgS3 = new pkgAWS.S3();

function generateS3RequestObject(payload, type) {
    const strS3Bucket = process.env.s3ServiceBucket; // in case we ever need to extend this to use additional buckets
    const strS3RootFolder = process.env.s3ServiceRootFolder; // in case we ever need to extend this to use additional root folders
    let strS3Key;
    if (type === 'PAGE') {
        strS3Key = `${strS3RootFolder}/${payload.editionGuid}/${payload.pageGuid}.pdf`;
    } else {
        strS3Key = `${strS3RootFolder}/${payload.editionGuid}/${payload.pageNumber}_${payload.pageGuid}.pdf`;
    }
    return {
        Bucket: strS3Bucket,
        Key: strS3Key.toLowerCase() // everything on AWS is case sensitive, so for sanity sake, lets stick with lowercase
    }
}

function generateS3DeleteObject(payload) {
    const strS3Bucket = process.env.s3ServiceBucket; // in case we ever need to extend this to use additional buckets
    const strS3RootFolder = process.env.s3ServiceRootFolder; // in case we ever need to extend this to use additional root folders
    let strS3Key;
    strS3Key = `${strS3RootFolder}/${payload}/`;
    return {
        Bucket: strS3Bucket,
        Prefix: strS3Key.toLowerCase() // everything on AWS is case sensitive, so for sanity sake, lets stick with lowercase
    }
}

async function listS3Items(payload) {
    let objS3Request = generateS3DeleteObject(payload);
    let listedItems = await pkgS3.listObjects(objS3Request).promise();
    return listedItems;
};


exports.checkOutputExists = async function (payload) {
    const objS3Request = generateS3RequestObject(payload, 'PAGE');

    let objResponse;
    try {
        objResponse = await pkgS3.headObject(objS3Request).promise();

        return {
            url: `https://s3-eu-west-1.amazonaws.com/${objS3Request.Bucket}/${objS3Request.Key}`,
            bucket: objS3Request.Bucket,
            key: objS3Request.Key
        };
    } catch (error) {
        console.log('HEAD ERROR');
        return null;
    }
}

exports.downloadOutput = async function (payload) {
    const objS3Request = generateS3RequestObject(payload, 'PAGE');

    const objS3Response = await pkgS3.getObject(objS3Request).promise();

    switch (outputType.toUpperCase()) {
        case 'BASE_HTML':
            return objS3Response['Body'].toString('utf8');
        case 'HOTSPOTS_JSON':
            return JSON.parse(objS3Response['Body'].toString('utf8'));
        default:
            return objS3Response['Body']; // Buffer
    }
}

exports.uploadOutput = async function (payload, uploadData, type) {
    let objS3Request = generateS3RequestObject(payload, type);
    objS3Request.ACL = 'public-read';
    objS3Request.ContentType = 'application/pdf';
    objS3Request.Body = uploadData;
    await pkgS3.putObject(objS3Request).promise();

    return {
        url: `https://s3-eu-west-1.amazonaws.com/${objS3Request.Bucket}/${objS3Request.Key}`,
        bucket: objS3Request.Bucket,
        key: objS3Request.Key
    };
};


exports.deleteObjects = async function (editionGuid) {
    let itemsDeleted = 0;
    try {
        let objList = await listS3Items(editionGuid);
        // console.log(objList);
        let items = objList.Contents;
        for (let i = 0; i < items.length; i += 1) {
            let deleteParams = { Bucket: process.env.s3ServiceBucket, Key: items[i].Key };
            let deleted = await pkgS3.deleteObject(deleteParams).promise();
            // console.log(deleteParams, deleted);
            itemsDeleted++;
        }

        return 'Items Deleted'

    } catch (error) {
        console.log('Delete ERROR');
        console.log(error);
        return null;
    }

};