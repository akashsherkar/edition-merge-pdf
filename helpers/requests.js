'use strict';

const pkgAxios = require('axios').default;

exports.getPDFBuffer = async function (pdfUrl) {
	return await axiosRequest({
		method: 'GET',
		url: pdfUrl,
		responseType: 'arraybuffer'
	});
}

exports.getPDFIds = async function (pdfUrl) {
	return await axiosRequest({
		method: 'GET',
		url: pdfUrl,
		headers: {
			'Content-Type': 'application/json',
			'x-api-key': '04QLFgxQxx21PiDQ6Y4GE6HxQQUsa1nU2b6MmKa6'
		},
		responseType: 'json'
	});
}

exports.getEditionDetails = async function (pdfUrl) {
	return await axiosRequest({
		method: 'GET',
		url: pdfUrl,
		headers: {
			'Content-Type': 'application/json',
			'x-api-key': '04QLFgxQxx21PiDQ6Y4GE6HxQQUsa1nU2b6MmKa6'
		},
		responseType: 'json'
	});
}

exports.getPageArticles = async function (pageUrl) {
	return await axiosRequest({
		method: 'GET',
		url: pageUrl,
		headers: {
			'Content-Type': 'application/json',
			'x-api-key': '04QLFgxQxx21PiDQ6Y4GE6HxQQUsa1nU2b6MmKa6'
		},
		responseType: 'json'
	});
}

async function axiosRequest(config) {
	let objResponse;

	try {
		objResponse = await pkgAxios(config);
	} catch (e) {
		outputError(e);
		throw e;
	}

	return objResponse;
}

function outputError(error) {
	if (typeof error.response !== 'undefined') {
		// The request was made and the server responded with a status code
		// that falls out of the range of 2xx
		console.log(error.response.data);
		console.log(error.response.status);
		console.log(error.response.headers);
	} else if (typeof error.request !== 'undefined') {
		// The request was made but no response was received
		// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
		// http.ClientRequest in node.js
		console.log(error.request);
	} else {
		// Something happened in setting up the request that triggered an Error
		console.log('Error', error.message);
	}
}