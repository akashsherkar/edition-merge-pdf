'use strict';

const pkgAWS = require('aws-sdk');
const pkgDynamoDb = new pkgAWS.DynamoDB.DocumentClient();

function generateTableRequestObject(payload) {

    return {
        TableName: process.env.dynamoDbTable,
        Key: {
            EditionGuid: payload
        },
    };
}

function generatePutRequest(payload) {
    return {
        TableName: process.env.dynamoDbTable,
        Item: {
            EditionGuid: payload.editionGuid,
            Status: 'PROCESSING',
            CreatedAt: payload.lastModified,
            UpdatedAt: payload.lastModified,
            S3Url: '',
            TotalJobs: payload.totalJobs,
            ExpirationDate: payload.expirationDate
        }
    }
}

function generateUpdateRequest(payload) {
    return {
        TableName: process.env.dynamoDbTable,
        Key:
        {
            "EditionGuid": payload.editionGuid
        },
        UpdateExpression: "set TotalJobs = TotalJobs - :val",
        ExpressionAttributeValues:
        {
            ":val": 1
        },
        ReturnValues: "UPDATED_NEW"
        // Key: {
        //     EditionGuid: payload.editionGuid
        // },
        // ExpressionAttributeNames: {
        //     '#status_text': 'Status',
        // },
        // ExpressionAttributeValues: {
        //     ':status': payload.status,
        //     ':s3Url': payload.s3Url,
        //     ':updatedAt': payload.lastUpdated,
        // },
        // UpdateExpression: 'SET #status_text = :status, S3Url = :s3Url, UpdatedAt = :updatedAt',
        // ReturnValues: 'ALL_NEW',
    }
}

exports.checkEditionExists = async function (payload) {
    const objDynamoRequest = generateTableRequestObject(payload);
    let objResponse;
    objResponse = await pkgDynamoDb.get(objDynamoRequest).promise();
    return objResponse;
}

exports.createRecord = async function (payload, lastUpdated) {
    let objDynamoRequest = generatePutRequest(payload, lastUpdated);
    let objResponse;
    objResponse = await pkgDynamoDb.put(objDynamoRequest).promise();
    return objResponse;
};

exports.updateRecord = async function (payload) {
    let objDynamoUpdateRequest = generateUpdateRequest(payload);
    let objResponse;
    objResponse = await pkgDynamoDb.update(objDynamoUpdateRequest).promise();
    return objResponse;
};