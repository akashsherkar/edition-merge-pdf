'use strict';
const helperSqs = require('../helpers/sqs');

module.exports.downloadTracker = async event => {
	for (const objRecord of event.Records) {
		console.log(objRecord);
		// we only care about MODIFY events as this will be the only event that will decrease the page count      
		if (objRecord.eventName !== 'MODIFY') {
			continue;
		}
		if (objRecord.dynamodb !== undefined && objRecord.dynamodb.NewImage !== undefined) {
			const objNewImage = objRecord.dynamodb.NewImage;
			console.log("Pages Left " + objNewImage.TotalJobs.N);
			if (objNewImage.TotalJobs.N !== undefined && objNewImage.TotalJobs.N == 0) {
				console.log("All Edition Processed");

				let payLoad = {
					edition: { guid: objNewImage.EditionGuid.S },
					s3: {
						bucket: process.env.s3ServiceBucket,
						key: `${process.env.s3ServiceRootFolder}/${objNewImage.EditionGuid.S}`
					}
				}
				console.log(payLoad);
				// Add message in Generate 
				await helperSqs.createSQSRecord(payLoad, 'editions_download_queue_group', process.env.editionGenerationQueue);
			}
		}
	}
	return;
}