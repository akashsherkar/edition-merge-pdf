'use strict';

const helperDynamo = require('../helpers/dynamodb');
const helperSqs = require('../helpers/sqs');
const helperRequests = require('../helpers/requests');
const helperS3 = require('../helpers/s3');

module.exports.initiateEditionProcessing = async (event) => {
    const strEditionGuid = event.pathParameters.editionGuid;
    const editionUrl = process.env.baseUrl + "editions/" + strEditionGuid;
    // Check for previous file in dynamodb
    const checkExistingPDFInDb = await helperDynamo.checkEditionExists(strEditionGuid);
    const editionLastUpdateObject = await helperRequests.getEditionDetails(editionUrl); // for checking last updates
    
    // Send Url if file found in Dynamo DB
    if (checkExistingPDFInDb.hasOwnProperty('Item') && checkExistingPDFInDb.Item.Status === 'FINISHED' && (editionLastUpdateObject.data.edition.lastModified === checkExistingPDFInDb.Item.UpdatedAt)) {

        return {
            statusCode: 200,
            body: JSON.stringify({
                s3Url: checkExistingPDFInDb.Item.S3Url,
                fileStatus: 'FINISHED',
                statusCode: 200
            })
        };

    } else if (checkExistingPDFInDb.hasOwnProperty('Item') && checkExistingPDFInDb.Item.Status === 'PROCESSING') {

        return {
            statusCode: 200,
            body: JSON.stringify({
                message: 'File processing is already in-progress',
                fileStatus: 'PROCESSING',
                statusCode: 200
            })
        };

    } else {
        let payLoad = {
            'lastModified': editionLastUpdateObject.data.edition.lastModified,
            'editionGuid': strEditionGuid,
            'random': Math.random()
        }

        await helperS3.deleteObjects(strEditionGuid); // Check for previous file in s3 bucket and delete them

        await helperSqs.createSQSRecord(payLoad, 'editions_download_queue_group', process.env.editionProcessQueue);

        console.log("Process Started");

        return {
            statusCode: 200,
            body: JSON.stringify({
                message: 'File processing has started',
                fileStatus: 'PROCESSING',
                statusCode: 200
            })
        };
    }
};
