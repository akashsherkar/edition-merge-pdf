'use strict';

const helperDynamo = require('../helpers/dynamodb');
const helperSqs = require('../helpers/sqs');
const helperRequests = require('../helpers/requests');
const moment = require('moment');

module.exports.triggerPageProcessing = async (event, context, callback) => {

    console.log("Stated processing Edition")

    for (const record of event.Records) {
        const { body } = record;

        var jsonBody = JSON.parse(body)

        try {

            const editionUrl = process.env.baseUrl + "editions/" + jsonBody.editionGuid + "/pages";
            let editionObjects = await helperRequests.getPDFIds(editionUrl); // get page list from edition
            let editionsPages = editionObjects.data.pages; // all pages

            let dynamoPayLoad = {
                'lastModified': jsonBody.lastModified,
                'editionGuid': jsonBody.editionGuid,
                'totalJobs': editionsPages.length,
                'expirationDate': moment().add(5, 'm').format('X')
            }

            await helperDynamo.createRecord(dynamoPayLoad); // Call DynamoDB to add the item to the table

            // Add all pages to SQS
            for (const page of editionsPages) {
                var jsonPayload = {
                    editionGuid: jsonBody.editionGuid,
                    url: process.env.s3DownloadUrl.replace('{char1}/{char2}/{pageGuid}', page.pageGuid.charAt(0) + '/' + page.pageGuid.charAt(1) + '/' + page.pageGuid),
                    random: Math.random(),
                    pageNumber: page.pageNumber
                }
                console.log(jsonPayload);
                await helperSqs.createSQSRecord(jsonPayload, 'pages_download_queue_group', process.env.pagesProcessQueue);
            }

        } catch (err) {
            console.log('*** ERROR ***'+ jsonBody.editionGuid);
            console.log(err);
            callback(error)
        } finally {
            console.log('*** All Pages triggered Successfully for '+ jsonBody.editionGuid +'***' );
            callback();
        }
    }

};
