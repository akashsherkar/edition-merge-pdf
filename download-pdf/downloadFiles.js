'use strict';

const helperS3 = require('../helpers/s3');
const helperDynamo = require('../helpers/dynamodb');
const helperRequests = require('../helpers/requests');
const PSPuppeteer = require('../classes/PSPuppeteer');

module.exports.downloadFiles = async (event, context, callback) => {

    console.log("Stated processing Pages")
    console.log("In One Lambda" + event.Records.length);

    for (const record of event.Records) {
        const { body } = record;

        var pageBody = JSON.parse(body)

        try {

            let uploadPayload = {
                'editionGuid': pageBody.editionGuid,
                'pageGuid': String(pageBody.pageNumber).padStart(4, "0")
            }
            
            const objPDFRequest = await helperRequests.getPDFBuffer(pageBody.url); // Get file from edition bucket as a buffer
            let pdfBuffer = objPDFRequest.data;
            await helperS3.uploadOutput(uploadPayload, pdfBuffer, 'PAGE'); // S3 put call to temp bucket

            // Get page URL's
            const getPageDetailsUrl = process.env.baseUrl + "editions/" + pageBody.editionGuid + "/page/" + pageBody.pageNumber + "/media";
            const pageArticles = await helperRequests.getPageArticles(getPageDetailsUrl); // Get file from edition bucket as a buffer

            for (const mediaFile of pageArticles.data.media) {

                if (mediaFile.type === 'ARTICLE') {

                    let articleUrl = process.env.articleBaseUrl + "popovers/dynamic_article_popover.aspx?artguid=" + mediaFile.fileGuid;

                    // lets create a new PageSuite Puppeteer instance
                    const objPSPuppeteer = new PSPuppeteer(articleUrl);
                    try {
                        // lets initialise PageSuite Puppeteer
                        await objPSPuppeteer.init();

                        // lets get the page dimensions for the page
                        await objPSPuppeteer.setupPageDimensions();

                        // lets get the PDF as a buffer
                        pdfBuffer = await objPSPuppeteer.generatePDFBuffer();

                        let objPayload = {
                            'editionGuid': pageBody.editionGuid,
                            'pageGuid': mediaFile.fileGuid,
                            'pageNumber': String(pageBody.pageNumber).padStart(4, "0")
                        }
                        console.log(objPayload);
                        // Put article in S3 bucket
                        await helperS3.uploadOutput(objPayload, pdfBuffer, 'ARTICLE');

                    } catch (err) {
                        console.log('*** ERROR ***');
                        console.log(err);

                        return {
                            statusCode: 500,
                            body: JSON.stringify(err)
                        };
                    } finally {
                        // lets always attempt to close our connection to Puppeteer                    
                        await objPSPuppeteer.close();
                    }

                }
            }

        } catch (error) {
            console.log('Processing ERROR' + pageBody.editionGuid);
            console.log(error);
            context.done(null, '');
        } finally {
            // Update DB on successful upload
            let dynamoPayLoad = {
                'editionGuid': pageBody.editionGuid,
            }
            await helperDynamo.updateRecord(dynamoPayLoad); // Call DynamoDB to add update item to the table 
            console.log('*** All Pages for ' + pageBody.editionGuid + ' transferred on S3***');
            callback();
        }
    }

};
